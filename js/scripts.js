$(function() {
  $('#vslider').vTicker('init', {
    showItems: 3,
    speed: 400
  });

});
$( "#prev" ).click(function() {
  $('#vslider').vTicker('prev', {animate:true});
});
$( "#next" ).click(function() {
  $('#vslider').vTicker('next', {animate:true});
});